from django.db import transaction
from rest_framework import serializers

from budget.models import Budget, Operation, Category
from core.serializers import UserReadSerializer


class BudgetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Budget
        fields = '__all__'
        extra_kwargs = {'owner': {'required': False}, 'participants': {'required': False}}

    def create(self, validated_data):
        request = self.context.get('request')
        validated_data['owner'] = request.user
        return super().create(validated_data)


class BudgetReadSerializer(serializers.ModelSerializer):
    participants = UserReadSerializer(many=True, read_only=True)

    class Meta:
        model = Budget
        fields = '__all__'


class CategoryReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['category_name', ]


class OperationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Operation
        fields = '__all__'
        extra_kwargs = {'user_id': {'required': False}, 'category': {'required': False}}

    @transaction.atomic
    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        if validated_data['value'] > 0 and validated_data['operation_type'] == Operation.WITHDRAW:
            validated_data['value'] *= -1
        instance = super().create(validated_data)
        first_category_found = None
        for keyword in [item.title() for item in instance.title.split()]:
            category = Category.objects.filter(keywords__contains=[keyword])
            if category:
                first_category_found = category.first()
                break
        instance.category = first_category_found or Category.objects.get(category_name='Inne wydatki')
        instance.save()
        return instance

    def to_representation(self, instance):
        return OperationReadSerializer(instance).data


class OperationReadSerializer(serializers.ModelSerializer):
    user = UserReadSerializer(read_only=True),
    budget = BudgetSerializer(read_only=True)
    category = CategoryReadSerializer(read_only=True)

    class Meta:
        model = Operation
        fields = '__all__'


class SimpleBudgetSerializer(serializers.Serializer):
    category = CategoryReadSerializer(read_only=True)
    created_at = serializers.DateTimeField()
    operation_type = serializers.CharField()
    value = serializers.FloatField()
    title = serializers.CharField()
    user = UserReadSerializer(read_only=True)
