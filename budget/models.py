from django.contrib.auth.models import User
from django.db import models
from django.contrib.postgres.fields import ArrayField

from core.models import BaseModel


class Budget(BaseModel):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_budgets')
    participants = models.ManyToManyField(User, related_name='budget_participate', blank=True)


class Category(models.Model):
    category_name = models.CharField(max_length=20)
    keywords = ArrayField(models.CharField(max_length=50), null=True)


class Operation(BaseModel):
    WITHDRAW = 'WITHDRAW'
    INCOME = 'INCOME'

    OPERATION_TYPE_CHOICES = (
        (WITHDRAW, 'Withdraw'),
        (INCOME, 'Income')
    )

    user = models.ForeignKey(User, related_name='user_operations', on_delete=models.SET_NULL, null=True)
    budget = models.ForeignKey(Budget, related_name='budget_operations', on_delete=models.CASCADE)
    operation_type = models.CharField(max_length=10, choices=OPERATION_TYPE_CHOICES)
    value = models.FloatField(default=0.0)
    title = models.TextField(blank=True)
    # i assumed that operation can have only one category
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, related_name='category_operations', null=True)