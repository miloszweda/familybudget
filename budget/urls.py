from rest_framework import routers

from budget.views import BudgetViewSet, OperationViewSet

router = routers.DefaultRouter()

router.register('budget', BudgetViewSet)
router.register('operation', OperationViewSet)