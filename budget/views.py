from django.db.models import Q, Sum
from django_filters import rest_framework as filters
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.response import Response

from rest_framework.viewsets import ModelViewSet

from budget.filters import OperationFilter
from budget.models import Budget, Operation
from budget.serializers import BudgetReadSerializer, BudgetSerializer, OperationSerializer, SimpleBudgetSerializer


class BudgetViewSet(ModelViewSet):
    queryset = Budget.objects.all()
    SERIALIZERS_MAP = {'list': BudgetReadSerializer, 'retrieve': BudgetReadSerializer}

    def get_queryset(self):
        return self.queryset.filter(Q(owner_id=self.request.user) | Q(participants__in=[self.request.user]))

    def get_serializer_class(self):
        return self.SERIALIZERS_MAP.get(self.action, BudgetSerializer)

    @action(detail=True, methods=('get', ))
    def get_budget_summary(self, request, *args, **kwargs):
        budget = self.get_object()
        budget_summary = budget.budget_operations.values('category').annotate(
            cin=Sum('value', filter=Q(operation_type=Operation.INCOME)),
            cout=Sum('value', filter=Q(operation_type=Operation.WITHDRAW)),
            summary=Sum('value')
        ).order_by()
        return Response(budget_summary, status=status.HTTP_200_OK)


class OperationViewSet(ModelViewSet):
    queryset = Operation.objects.all().order_by('-created_at')
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = OperationFilter

    SERIALIZERS_MAP = {'list': SimpleBudgetSerializer, 'retrieve': SimpleBudgetSerializer}

    def get_serializer_class(self):
        return self.SERIALIZERS_MAP.get(self.action, OperationSerializer)

    def destroy(self, request, *args, **kwargs):
        raise MethodNotAllowed('delete')

    def partial_update(self, request, *args, **kwargs):
        raise MethodNotAllowed('patch')

