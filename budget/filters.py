import django_filters

from budget.models import Operation


class OperationFilter(django_filters.FilterSet):
    class Meta:
        model = Operation
        fields = ['user_id', 'operation_type', 'category_id', 'budget']