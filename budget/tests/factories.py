import factory

from budget.models import Budget, Category, Operation
from core.tests.factories import UserFactory


class BudgetFactory(factory.django.DjangoModelFactory):
    owner = factory.SubFactory(UserFactory)

    @factory.post_generation
    def participants(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for participant in extracted:
                self.participants.add(participant)

    class Meta:
        model = Budget


class CategoryFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Category


class OperationFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    budget = factory.SubFactory(BudgetFactory)
    category = factory.SubFactory(CategoryFactory)

    class Meta:
        model = Operation
