import pytest
from rest_framework import status

from budget.tests.factories import BudgetFactory, OperationFactory
from core.tests.factories import UserFactory


@pytest.mark.django_db
class TestBudgetView:

    def test_budget_create(self, authorized_client):
        response = authorized_client.post('/budget/')
        assert response.status_code == status.HTTP_201_CREATED
        assert response.json()['owner'] == authorized_client.user.id

    def test_add_budget_participants(self, authorized_client):
        users = [UserFactory().id for _ in range(2)]
        response = authorized_client.post('/budget/', data={'participants': users})
        assert response.status_code == status.HTTP_201_CREATED
        assert len(response.json()['participants']) == len(users)

    def test_get_budget(self, authorized_client):
        budget = BudgetFactory(owner=authorized_client.user)
        response = authorized_client.get(f'/budget/{budget.id}/')
        assert response.status_code == status.HTTP_200_OK
        assert response.json()['owner'] == authorized_client.user.id

    def test_delete_budget(self, authorized_client):
        budget = BudgetFactory(owner=authorized_client.user)
        response = authorized_client.delete(f'/budget/{budget.id}/')
        assert response.status_code == status.HTTP_204_NO_CONTENT

    def test_get_budget_summary(self, authorized_client, category_fixture):
        budget = BudgetFactory(owner=authorized_client.user)
        operations = [OperationFactory(budget=budget, user=authorized_client.user, value=100, operation_type='INCOME',
                                       category=category_fixture[0]) for _ in range(3)]
        response = authorized_client.get(f'/budget/{budget.id}/get_budget_summary/')
        assert response.status_code == status.HTTP_200_OK
        assert response.json()[0]['category'] == category_fixture[0].id
        assert response.json()[0]['cin'] == 300
        assert response.json()[0]['summary'] == 300

    def test_get_budget_summary_with_minus(self, authorized_client, category_fixture):
        budget = BudgetFactory(owner=authorized_client.user)
        operations = [OperationFactory(budget=budget, user=authorized_client.user, value=100, operation_type='INCOME',
                                       category=category_fixture[0]) for _ in range(3)]
        operations_min = [OperationFactory(budget=budget, user=authorized_client.user, value=-50, operation_type='WITHDRAW',
                                       category=category_fixture[0]) for _ in range(3)]
        response = authorized_client.get(f'/budget/{budget.id}/get_budget_summary/')
        assert response.status_code == status.HTTP_200_OK
        assert response.json()[0]['category'] == category_fixture[0].id
        assert response.json()[0]['cin'] == 300
        assert response.json()[0]['cout'] == -150
        assert response.json()[0]['summary'] == 150

    def test_budget_participate(self, authorized_client, authorized_user):
        budget = BudgetFactory(owner=authorized_client.user)
        budget.participants.set([authorized_user.user])
        response = authorized_user.get('/budget/')
        assert response.status_code == status.HTTP_200_OK
        assert response.json()['count'] == 1


@pytest.mark.django_db
class TestOperationView:

    def test_create_operation(self, authorized_client):
        budget = BudgetFactory(owner=authorized_client.user)
        payload = {'budget': budget.id, 'operation_type': 'WITHDRAW', 'value': 100.00, 'title': 'opłata za Internet'}
        response = authorized_client.post('/operation/', data=payload)
        assert response.status_code == status.HTTP_201_CREATED
        assert response.json()['value'] == payload['value'] * -1
        assert response.json()['category']['category_name'] == 'Media'

    def test_list_operations(self, authorized_client):
        budget = BudgetFactory(owner=authorized_client.user)
        operations = [OperationFactory(budget=budget, user=authorized_client.user) for _ in range(3)]
        response = authorized_client.get('/operation/')
        assert response.status_code == status.HTTP_200_OK
        assert response.json()['count'] == len(operations)

    def test_delete_operation(self, authorized_client):
        budget = BudgetFactory(owner=authorized_client.user)
        operations = OperationFactory(budget=budget, user=authorized_client.user)
        response = authorized_client.delete(f'/operation/{operations.id}/')
        assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED

    def test_update_operation(self, authorized_client):
        budget = BudgetFactory(owner=authorized_client.user)
        operations = OperationFactory(budget=budget, user=authorized_client.user)
        response = authorized_client.patch(f'/operation/{operations.id}/', data={'budget': budget.id+1, })
        assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED

