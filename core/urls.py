from rest_framework import routers

from core.views import UserRegistrationAPIView, UserViewSet, LoginApiView

router = routers.DefaultRouter()

router.register(r'register', UserRegistrationAPIView, basename='user_register')
router.register(r'login', LoginApiView, basename='user_login')
router.register(r'users', UserViewSet)