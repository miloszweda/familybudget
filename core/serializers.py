import time
from datetime import datetime, timedelta

from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from rest_framework import serializers

import jwt


class UserRegistrationSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = '__all__'

    def create(self, validated_data):
        instance = super().create(validated_data)
        instance.set_password(validated_data['password'])
        instance.save()
        return instance

    def to_representation(self, instance):
        return UserReadSerializer(instance).data


# we dont want to return password
class UserReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'is_active']


class UserLoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def create(self, validated_data):
        user = authenticate(username=validated_data['username'], password=validated_data['password'])
        if not any([user or user.is_active]):
            raise ValueError('Given credentials are wrong or user has been deactivated')
        return self.create_token(user)

    def create_token(self, user):
        dt = datetime.now() + timedelta(days=1)
        return jwt.encode({'id': user.id, 'exp': int(time.mktime(dt.timetuple()))},
                          settings.SECRET_KEY, algorithm='HS256')