from random import randint
import factory
from django.contrib.auth.models import User
from factory.fuzzy import FuzzyInteger


class UserFactory(factory.django.DjangoModelFactory):
    username = factory.Sequence(lambda o: f'user{randint(0, 1000)}{randint(0, 1000)}')
    is_active = FuzzyInteger(1, 1)
    password = factory.PostGenerationMethodCall('set_password', 'user123')

    class Meta:
        model = User
