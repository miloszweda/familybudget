import pytest
from rest_framework import status

from core.tests.factories import UserFactory


@pytest.mark.django_db
class TestUserView:

    def test_user_register(self, unauthorized_client):
        payload = {'username': 'testuser', 'password': 'user123'}
        response = unauthorized_client.post('/register/', data=payload)
        assert response.status_code == status.HTTP_201_CREATED
        assert response.json()['username'] == payload['username']

    def test_user_login(self, unauthorized_client):
        user = UserFactory(username='testuser', is_active=1)
        response = unauthorized_client.post('/login/', data={'username':  user.username, 'password': 'user123'})
        print(response.json())
        assert response.status_code == status.HTTP_200_OK
        assert len(response.json()) > 0

    def test_user_update(self, authorized_client):
        user = UserFactory(username='testuser', is_active=1)
        response = authorized_client.patch(f'/users/{user.id}/', data={'username': 'updated'})
        assert response.status_code == status.HTTP_200_OK
        assert response.json()['username'] == 'updated'

    def test_user_delete(self, authorized_client):
        user = UserFactory(username='testuser', is_active=1)
        response = authorized_client.delete(f'/users/{user.id}/')
        assert response.status_code == status.HTTP_204_NO_CONTENT

    def test_list_users(self, authorized_client):
        users = [UserFactory() for _ in range(10)]
        response = authorized_client.get('/users/')
        assert response.status_code == status.HTTP_200_OK
        assert len(response.json()['results']) == len(users)


