from django.contrib.auth.models import User
from django_filters import rest_framework as filters
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, ViewSet

from core.filters import UserFilter
from core.serializers import UserRegistrationSerializer, UserLoginSerializer, UserReadSerializer


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = UserFilter
    allowed_http_methods = ['get', 'head', 'options']

    SERIALIZERS_MAP = {
        'list': UserReadSerializer
    }

    def get_serializer_class(self):
        return self.SERIALIZERS_MAP.get(self.action, UserReadSerializer)


class UserRegistrationAPIView(ViewSet):
    serializer_class = UserRegistrationSerializer
    authentication_classes = []
    permission_classes = []

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()
        return Response(serializer.to_representation(instance), status=status.HTTP_201_CREATED)


class LoginApiView(ViewSet):
    serializer_class = UserLoginSerializer
    authentication_classes = []
    permission_classes = []

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.save(), status=status.HTTP_200_OK)
