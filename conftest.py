import pytest
from django.contrib.auth.models import User
from rest_framework.test import APIClient

from budget.tests.factories import CategoryFactory


@pytest.fixture
def authorized_client():
    client = APIClient()
    user = User.objects.create(username='clientuser')
    setattr(client, 'user', user)
    client.force_authenticate(user=user)
    return client


@pytest.fixture
def authorized_user():
    client = APIClient()
    user = User.objects.create(username='support_user')
    setattr(client, 'user', user)
    client.force_authenticate(user=user)
    return client


@pytest.fixture
def unauthorized_client():
    return APIClient()


@pytest.fixture
def category_fixture():
    categories_data = [
        {'category_name': 'Inne wydatki', 'keywords': []},
        {'category_name': 'Transport', 'keywords': ['Paliwo', 'Przejazd', 'Bilet']},
        {'category_name': 'Media', 'keywords': ['Prąd', 'Woda', 'Internet', 'Gaz']},
        {'category_name': 'Zakupy', 'keywords': ['Zakupy', ]}
    ]
    return [CategoryFactory(**item) for item in categories_data]